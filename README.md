# README #

Simple example of vue app working within a django app

### What is this repository for? ###

* Run Django dev server on port 8000
* Run Webpack Dev Server on port 8080
* Render Vue app within the django template to get use of django context variables
* Have Webpack do seamless reloads when you update your Vue code
* Build production js files to django accessible location

### How do I get set up? ###
```
 git clone https://bjfinnerty@bitbucket.org/bjfinnerty/django_vue_sample.git
 cd django_vue_sample
 pip install -r requirements.txt
 cd hellovue/vuesample/
 npm install
 npm start
 http://localhost:8001/hello-vue/
```